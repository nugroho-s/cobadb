/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cobadb;

/**
 *
 * @author Nugroho <massatriya@gmail.com>
 */
public class user {
    int id;
    String first_name;
    String last_name;
    String email;
    String username;
    String gender;
    
    public user(int id, String first_name,String last_name,String email, String username, String gender){
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.username = username;
        this.gender = gender;
    }
}
